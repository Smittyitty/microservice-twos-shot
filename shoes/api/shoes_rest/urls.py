from django.urls import path

from .views import ShoeList, ShoeDetail

urlpatterns = [
    path("shoes/", ShoeList, name="show_shoe_list"),
    path("shoes/<int:pk>/",ShoeDetail, name="show_shoe_detail",),
]
