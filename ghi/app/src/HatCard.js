import React, { useEffect, useState } from 'react';


function HatCard(props) {
    if (props.hat === undefined) {
        return null;
      }

    return (
        <>
        <div className="col">
            <div key={props.hat.style_name} className="card mb-3 shadow">
                <img src={props.hat.picture_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">Style name: {props.hat.style_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    Fabric: {props.hat.fabric}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                    Color: {props.hat.color}
                </h6>
                </div>
                <div className="card-footer">
                    Location: {props.hat.location.name} 

                </div>
                <div className="card-footer">
                <button onClick={props.deleteHat} type="button" className="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
        </>

    )

}

export default HatCard