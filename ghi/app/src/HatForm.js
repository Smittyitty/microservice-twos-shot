import React, { useEffect, useState } from 'react';

function HatForm() {
    const [styleName, setStyle] = useState('');
    const [fabricName, setFabric] = useState('');
    const [ColorName, setColor] = useState('');
    const [URL, setURL] = useState('');
    const [theLocation, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    const handleSubmit = async (event) => {
      event.preventDefault();
    
      const data = {
        style_name: styleName,
        color: ColorName,
        fabric: fabricName,
        picture_url: URL,
        location: theLocation,
      };
    
    
      const hatURL = 'http://localhost:8090/api/hats/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

          const response = await fetch(hatURL, fetchConfig);
          if (response.ok) {
            const newHat = await response.json();

            setStyle('');
            setFabric('');
            setColor('');
            setURL('');
            setLocation('');
          }
      }

    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);

    }

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);

    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);

    }


    const handleURLChange = (event) => {
        const value = event.target.value;
        setURL(value);

    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);

    }



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';


        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);



        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleStyleChange} value={styleName} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control"/>
                <label htmlFor="style_name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFabricChange} value={fabricName} placeholder="Fabric Name" required type="text" name="fabric_name" id="fabric_name" className="form-control"/>
                <label htmlFor="fabric_name">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={ColorName} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleURLChange} value={URL} placeholder="Hat picture url" required type="text" name="hat_picture_url" id="fabric_name" className="form-control"/>
                <label htmlFor="hat_picture_url">Hat Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} value={theLocation} required name="location" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>
                            {location.closet_name}: Section: {location.section_number} Shelf: {location.shelf_number}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default HatForm;