import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ShoeCard(props) {
    if (props.shoe === undefined) {
        return null;
      }

    return (
        <>
        <div className="col">
            <div key={props.shoe.model_name} className="card mb-3 shadow">
                <img src={props.shoe.picture_url} className="card-img-top" />
                <div className="card-body">
                <h5 className="card-title">Model name: {props.shoe.model_name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">
                    Manufacturer: {props.shoe.manufacturer}
                </h6>
                <h6 className="card-subtitle mb-2 text-muted">
                    Color: {props.shoe.color}
                </h6>
                </div>
                <div className="card-footer">
                    Bin: {props.shoe.bin.name} 

                </div>
                <div className="card-footer">
                <button onClick={props.deleteShoe} type="button" className="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
        </>

    )

}

export default ShoeCard