import React, { useEffect, useState } from 'react';
import HatCard from './HatCard';


function HatList() {
    const [hatCards, setHatCards] = useState([]);
    const deleteHat = async (index, id) => {
        const hatURL = 'http://localhost:8090/api/hats/' + id
        const fetchConfig = {
            method: "delete",

          };


        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            const Something= hatCards.filter(function (currentElement, i) {
                return i !== index;
            })

            setHatCards(Something);


        }



    }

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';


        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHatCards(data.hats)

        }





    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <>
        <div className="container mt-4">
        <h2>A list of hats</h2>
        <div className="row row-cols-3">
          {hatCards.map((hatCard, index) => {
            return (
              <HatCard key={index} index={index} hat={hatCard} updateState={setHatCards} deleteHat={()=>deleteHat(index, hatCard.id)} hatCardsList={hatCards}/>
            );
          })}
        </div>
      </div>

        </>
    )



}

export default HatList;