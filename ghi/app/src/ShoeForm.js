import React, { useEffect, useState } from 'react';

function ShoeForm() {
    const [modelName, setModel] = useState('');
    const [manufacturerName, setManu] = useState('');
    const [ColorName, setColor] = useState('');
    const [URL, setURL] = useState('');
    const [theBin, setBin] = useState('');
    const [bins, setBins] = useState([]);

    const handleSubmit = async (event) => {
      event.preventDefault();
    
      const data = {
        model_name: modelName,
        color: ColorName,
        manufacturer: manufacturerName,
        picture_url: URL,
        bin: theBin,
      };
    
    
      const shoeURL = 'http://localhost:8080/api/shoes/';
      const fetchConfig = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

          const response = await fetch(shoeURL, fetchConfig);
          if (response.ok) {
            const newShoe = await response.json();

            setModel('');
            setManu('');
            setColor('');
            setURL('');
            setBin('');
          }
      }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);

    }

    const handleManuChange = (event) => {
        const value = event.target.value;
        setManu(value);

    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);

    }


    const handleURLChange = (event) => {
        const value = event.target.value;
        setURL(value);

    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);

    }



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';


        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);



        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleModelChange} value={modelName} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                <label htmlFor="model_name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleManuChange} value={manufacturerName} placeholder="Manufacturer Name" required type="text" name="manufacturer_name" id="manufacturer_name" className="form-control"/>
                <label htmlFor="manufacturer_name">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={ColorName} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleURLChange} value={URL} placeholder="Shoe picture url" required type="text" name="shoe_picture_url" id="manufacturer_name" className="form-control"/>
                <label htmlFor="shoe_picture_url">Shoe Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleBinChange} value={theBin} required name="bin" id="bin" className="form-select">
                  <option value="">Choose a bin</option>
                  {bins.map(bin => {
                    return (
                        <option value={bin.id} key={bin.id}>
                            {bin.closet_name} bin#: {bin.bin_number} Bin size: {bin.bin_size}
                        </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ShoeForm;
