import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React from 'react';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';
import HatList from './HatList';

function App(props) {
  return (
    <Router>
      <Nav />
        <Routes>

          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
              <Route index element={<ShoeList/>} />
              <Route path="new" element={<ShoeForm />} />
          </Route>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
              <Route index element={<HatList/>} />
              <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
    </Router>
  );
}

export default App;