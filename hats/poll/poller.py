import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something

from hats_rest.models import LocationVO


def getLocation():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    print(content)
    for location in content["locations"]:
        try:
            obj, created = LocationVO.objects.update_or_create(
                import_href=location["href"],
                defaults={"name": location["closet_name"]}
            )
            if created:
                print("Created locationVO object:", obj)

            else:
                print("Updated object:", obj)

        except Exception as e:
            print("Got an error:", e)




def poll():
    while True:
        print('Hats poller polling for data')
        try:
            getLocation()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
