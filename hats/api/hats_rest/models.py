from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Hat(models.Model):
    fabric = models.CharField(max_length= 150)
    color = models.CharField(max_length=20)
    style_name = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)


    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.color} {self.style_name} Hat at {self.location}"

    def get_api_url(self):
        return reverse("show_hat_detail", kwargs={"pk": self.pk})