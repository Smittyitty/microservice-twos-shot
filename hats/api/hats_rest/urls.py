from django.urls import path

from .views import HatList, HatDetail

urlpatterns = [
    path("hats/", HatList, name="show_hat_list"),
    path("hats/<int:pk>/",HatDetail, name="show_hat_detail",),
]
