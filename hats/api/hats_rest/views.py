from django.http import JsonResponse, HttpResponseBadRequest, Http404
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name", "import_href"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric",
                  "style_name",
                  "id",
                  "color",
                  "picture_url",
                  "location",
                  ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["fabric",
                  "color",
                  "style_name",
                  "picture_url",
                  "location"
                  ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def HatList(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return HttpResponseBadRequest("Invalid JSON data")

        try:
            location_href = f"/api/locations/{content['location']}/"
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        try:
            hat = Hat.objects.create(**content)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)
    else:
        return HttpResponseBadRequest("Invalid HTTP method")

@require_http_methods(["GET", "POST", "DELETE"])
def HatDetail(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder = HatDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
